import { defineStore } from "pinia";
import { useLocalStorage } from "@vueuse/core";

export const useTaskStore = defineStore('taskStore', {
  state: () => {
    return {
      tasks: useLocalStorage('tasks', []),
    };
  },
  getters: {
    favs() {
      return this.tasks.filter(task => task.isFav);
    },
    favCount() {
      return this.favs.length;
    },
    taskCount() {
    return this.tasks.length;
    },
  },
  actions: {
    addTask(task) {
      this.tasks.push(task);
    },
    deleteTask(id) {
      this.tasks = this.tasks.filter(task => task.id !== id);
    },
    toggleFav(id) {
      const task = this.tasks.find(task => task.id === id);
      task.isFav = !task.isFav;
    },
  },
});
